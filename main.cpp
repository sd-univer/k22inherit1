#include <iostream>

class Animal {
private:
    std::string name;

protected:
    int weight;

public:
    Animal() {
        std::cout << "Animal ctor - empty" << std::endl;
    }

    Animal(std::string name) : name(name), weight(10)  {
        std::cout << "Animal ctor - with name" << std::endl;
        // this->name = name;
    }

    void greet() {
        std::cout << "Hi from " << name << std::endl;
    }
};

// class Cat(Animal) - python
class Cat : public Animal {
private:
    int fur_length = 1;
public:
    Cat() {
        std::cout << "Cat ctor - empty" << std::endl;
    }

    Cat(std::string name) : fur_length(1), Animal(name) {
        std::cout << "Cat ctor - with name" << std::endl;
    }

    Cat(std::string name, int fur_length) : fur_length(fur_length), Animal(name) {
        std::cout << "Cat ctor - with name" << std::endl;
    }

    int getFurLength() {
        return fur_length;
    }
};

int main() {
    Animal a("The First Ani");
    a.greet();

    Cat cat("Murchyk", 10);
    cat.greet();
    std::cout << cat.getFurLength();

    return 0;
}
